from flask import Flask
app = Flask(__name__)


@app.route('/generate_master_playlist')
def generate_master_playlist():
    return 'master'


@app.route('/generate_sub_playlist')
def generate_sub_playlist():
    return 'sub_playlist'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
